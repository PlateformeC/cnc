#!/usr/bin/python
# -*- coding: latin-1 -*-

from tkinter import *
from tkinter import ttk
import math

def calculsCP():
	diam = saisieDiamFCP.get()
	angl = saisieAngleCP.get()
	
	if diam =='' or angl=='':
		ResultProfFraisage.set('valeur vide')
	else:
		dfraise=float(diam)
		angle=float(angl)
		demi_angle=angle/2
		demi_angle_r=math.radians(demi_angle)
		rfraise=dfraise/2
		prof_fraisage=rfraise/math.tan(demi_angle_r)
		ResultProfFraisage.set('%.3f' % prof_fraisage)

def calculsCD():
	prof = saisieAngleCD.get()
	angl = saisieProfFCD.get()
	if prof =='' or angl=='':
		ResultDiamFraisage.set('valeur vide')
	else:
		pfraise=float(prof)
		angle=float(angl)
		demi_angle=angle/2
		demi_angle_r=math.radians(demi_angle)
		rfraise=pfraise*math.tan(demi_angle_r)
		dfraise=rfraise*2
		ResultDiamFraisage.set('%.3f' % dfraise)

Kalkulator = Tk()     	 # Création fenêtre
Kalkulator.title('Kalkulator')

# Calcul de profondeur de fraisage
ResultProfFraisage=StringVar()

nomCP = Label(Kalkulator, text='Calcul de profondeur de fraisage')
nomCP.grid(row=0, column=0, columnspan=2, sticky=W, padx=10, pady=20)

legendeAngleCP = Label(Kalkulator, text='Angle (degres) : ')
legendeAngleCP.grid(row=1, column=0, sticky=W, padx=10, pady=10) 

saisieAngleCP = Entry(Kalkulator, textvariable=StringVar(), width=12)
saisieAngleCP.grid(row=1, column=1, sticky=W, padx=10)

legendeDiamFCP = Label(Kalkulator, text='Diam fraise (mm) : ')
legendeDiamFCP.grid(row=2, column=0, sticky=W, padx=10, pady=10)
  
saisieDiamFCP = Entry(Kalkulator, textvariable=StringVar(), width=12)
saisieDiamFCP.grid(row=2, column=1, sticky=W, padx=10)

legendeResultProfFCP = Label(Kalkulator, text='Profondeur de fraisage : ')
legendeResultProfFCP.grid(row=3, column=0, sticky=W, padx=10, pady=10)  

ResultProfFCP = Label(Kalkulator, textvariable=ResultProfFraisage,  relief=RIDGE, width=12)
ResultProfFCP.grid(row=3, column=1, sticky=W, padx=10, pady=10)


BoutonCalculCP = Button(Kalkulator, text='Calcul', command=calculsCP)
BoutonCalculCP.grid(row=1, column=2, rowspan=3, sticky=E, padx=10, pady=10)

# Calcul de diametre de fraisage
ResultDiamFraisage=StringVar()

nomCD = Label(Kalkulator, text='Calcul de diametre de fraisage')
nomCD.grid(row=5, column=0, columnspan=2, sticky=W, padx=10, pady=20)

legendeAngleCD  = Label(Kalkulator, text='Angle (degres) : ')
legendeAngleCD.grid(row=6, column=0, sticky=W, padx=10, pady=10)  

saisieAngleCD = Entry(Kalkulator, textvariable=StringVar(), width=12)
saisieAngleCD.grid(row=6, column=1, sticky=W, padx=10)

legendeProfFCD = Label(Kalkulator, text='Profondeur fraise (mm) : ')
legendeProfFCD.grid(row=7, column=0, sticky=W, padx=10, pady=10) 
 
saisieProfFCD = Entry(Kalkulator, textvariable=StringVar(), width=12)
saisieProfFCD.grid(row=7, column=1, sticky=W, padx=10)


legendeResultDiamFCD = Label(Kalkulator, text='Diametre de fraisage : ')
legendeResultDiamFCD.grid(row=8, column=0, sticky=W, padx=10, pady=10)  
ResultDiamFCD = Label(Kalkulator, textvariable=ResultDiamFraisage,  relief=RIDGE, width=12)
ResultDiamFCD.grid(row=8, column=1, sticky=W, padx=10, pady=10)

BoutonCalculCD = Button(Kalkulator, text='Calcul', command=calculsCD)
BoutonCalculCD.grid(row=6, column=2, rowspan=3, sticky=E, padx=10, pady=10)
# ----


BoutonQuitter = Button(Kalkulator, text='Quitter', command=Kalkulator.destroy)
BoutonQuitter.grid(row=9, column=2, columnspan=1, sticky=E, padx=10, pady=10)

Kalkulator.mainloop()  	 # Boucle principale

